<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return Product::with('images', 'classification')->get();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'classification_id' => 'required|exists:classifications,id',
            'images' => 'nullable|array',
            'images.*' => 'image'
        ]);

        $product = Product::create($request->only('name', 'price', 'classification_id'));

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $path = $image->store('images');
                $product->images()->create(['path' => $path]);
            }
        }

        return response()->json($product->load('images', 'classification'), 201);
    }

    public function show(Product $product)
    {
        return $product->load('images', 'classification');
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'classification_id' => 'required|exists:classifications,id',
            'images' => 'nullable|array',
            'images.*' => 'image'
        ]);

        $product->update($request->only('name', 'price', 'classification_id'));

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $path = $image->store('images');
                $product->images()->create(['path' => $path]);
            }
        }

        return response()->json($product->load('images', 'classification'), 200);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json(null, 204);
    }
}
