<?php

namespace App\Http\Controllers;

use App\Models\Classification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClassificationController extends Controller
{
    public function index()
    {
        $classifications = Classification::with('images')->get();
        return response()->json($classifications);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:classifications,name',
            'image' => 'nullable|image|max:2048'
        ]);

        $classification = Classification::create($request->only('name'));

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('classifications');
            $classification->images()->create(['path' => $path]);
        }

        return response()->json($classification->load('images'), 201);
    }

    public function show(Classification $classification)
    {
        return response()->json($classification->load('images'));
    }

    public function update(Request $request, Classification $classification)
    {
        $request->validate([
            'name' => 'required|string|unique:classifications,name,' . $classification->id,
            'image' => 'nullable|image|max:2048'
        ]);

        $classification->update($request->only('name'));

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('classifications');
            $classification->images()->create(['path' => $path]);
        }

        return response()->json($classification->load('images'), 200);
    }

    public function destroy(Classification $classification)
    {
        $classification->images->each(function ($image) {
            Storage::delete($image->path);
            $image->delete();
        });
        $classification->delete();

        return response()->json(null, 204);
    }
}
