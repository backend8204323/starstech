<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'classification_id',
        'image',
    ];
    
    public function classification()
    {
        return $this->belongsTo(Classification::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
