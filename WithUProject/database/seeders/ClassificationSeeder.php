<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Classification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ClassificationSeeder extends Seeder
{
    public function run()
    {
        $classifications = [
            ['name' => 'Electronics'],
            ['name' => 'Furniture'],
            ['name' => 'Clothing'],
        ];

        foreach ($classifications as $classification) {
            if (!Classification::where('name', $classification['name'])->exists()) {
                $imagePath = 'classifications/' . Str::uuid() . '.jpg';
                $localPlaceholderPath = storage_path('app/public/images/placeholders/placeholder.jpg');

                // Ensure the file exists before attempting to copy it
                if (file_exists($localPlaceholderPath)) {
                    // Copy the local placeholder image to the storage path
                    Storage::disk('public')->put($imagePath, file_get_contents($localPlaceholderPath));
                } else {
                    throw new \Exception("Placeholder image not found at: {$localPlaceholderPath}");
                }

                Classification::create([
                    'name' => $classification['name'],
                    'image' => $imagePath,
                ]);
            }
        }
    }
}
