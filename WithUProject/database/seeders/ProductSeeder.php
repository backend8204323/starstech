<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Classification;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $classifications = Classification::all();
        $localPlaceholderPath = storage_path('app/public/images/placeholders/placeholder.jpg');

        foreach ($classifications as $classification) {
            for ($i = 0; $i < 10; $i++) {  // Corrected line
                $imagePath = 'products/' . Str::uuid() . '.jpg';

                // Ensure the file exists before attempting to copy it
                if (file_exists($localPlaceholderPath)) {
                    // Copy the local placeholder image to the storage path
                    Storage::disk('public')->put($imagePath, file_get_contents($localPlaceholderPath));
                } else {
                    throw new \Exception("Placeholder image not found at: {$localPlaceholderPath}");
                }

                Product::create([
                    'name' => $faker->word,
                    'price' => $faker->randomFloat(2, 10, 1000),
                    'classification_id' => $classification->id,
                    'image' => $imagePath,
                ]);
            }
        }
    }
}
