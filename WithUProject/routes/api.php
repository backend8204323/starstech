<?php

use App\Http\Controllers\ClassificationController;
use App\Http\Controllers\ProductController;

// Classifications Routes
Route::group(['prefix' => 'v1/classifications'], function () {
    Route::get('/', [ClassificationController::class, 'index'])->name('classifications.index');
    Route::post('/', [ClassificationController::class, 'store'])->name('classifications.store');
    Route::get('{classification}', [ClassificationController::class, 'show'])->name('classifications.show');
    Route::put('{classification}', [ClassificationController::class, 'update'])->name('classifications.update');
    Route::delete('{classification}', [ClassificationController::class, 'destroy'])->name('classifications.destroy');
});

// Products Routes
Route::group(['prefix' => 'v1/products'], function () {
    Route::get('/', [ProductController::class, 'index'])->name('products.index');
    Route::post('/', [ProductController::class, 'store'])->name('products.store');
    Route::get('{product}', [ProductController::class, 'show'])->name('products.show');
    Route::put('{product}', [ProductController::class, 'update'])->name('products.update');
    Route::delete('{product}', [ProductController::class, 'destroy'])->name('products.destroy');
});
